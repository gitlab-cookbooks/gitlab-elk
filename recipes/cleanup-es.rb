#
# Cookbook Name:: gitlab-elk
# Recipe:: cleanup-es
#
# Copyright 2016, GitLab Inc.
#
# All rights reserved - Do Not Redistribute
#
cron 'daily cleanup elastic indices' do
  command '/usr/local/bin/cleanup-es.sh'
  minute '5'
  hour '1'
  path '/usr/local/sbin:/usr/sbin/:/sbin:/usr/local/bin:/usr/bin:/bin'
end

template '/usr/local/bin/cleanup-es.sh' do
  owner  'root'
  group  'root'
  mode   '0755'
end

cron 'daily cleanup elasticsearch logs' do
  command '/usr/local/bin/cleanup-es-logs.sh'
  minute '30'
  hour '2'
  path '/usr/local/sbin:/usr/sbin/:/sbin:/usr/local/bin:/usr/bin:/bin'
end

cookbook_file '/usr/local/bin/cleanup-es-logs.sh' do
  source 'cleanup-es-logs.sh'
  owner 'root'
  group 'root'
  mode '0755'
end
