include_recipe "runit::default"

node.default["gitlab-elk"] =
  if node['gitlab-elk']['chef_vault']
    include_recipe 'gitlab-vault'
    GitLab::Vault.get(node, 'gitlab-elk')
  else
    secrets_hash = node['gitlab-elk']['secrets']
    node_secrets = get_secrets(secrets_hash['backend'], secrets_hash['path'], secrets_hash['key'])
    Chef::Mixin::DeepMerge.deep_merge(node_secrets['gitlab-elk'], node['gitlab-elk'].to_hash)
  end

directory node["gitlab-elk"]["pubsubbeat"]["home_dir"] do
  owner node["gitlab-elk"]["pubsubbeat"]["user"]
  group node["gitlab-elk"]["pubsubbeat"]["group"]
  mode "0700"
  recursive true
end

include_recipe "ark::default"

dir_name = ::File.basename(node["gitlab-elk"]["pubsubbeat"]["home_dir"])
dir_path = ::File.dirname(node["gitlab-elk"]["pubsubbeat"]["home_dir"])

ark dir_name do
  url node["gitlab-elk"]["pubsubbeat"]["url"]
  checksum node["gitlab-elk"]["pubsubbeat"]["sha256sum"]
  version node["gitlab-elk"]["pubsubbeat"]["version"]
  prefix_root Chef::Config["file_cache_path"]
  path dir_path
  owner node["gitlab-elk"]["pubsubbeat"]["user"]
  group node["gitlab-elk"]["pubsubbeat"]["group"]
  action :put
  notifies :restart, "runit_service[pubsubbeat]", :delayed
end

conf_content = node["gitlab-elk"]["pubsubbeat"]["conf"].to_hash
conf_content["pubsubbeat"]["json.enabled"] = false if node["gitlab-elk"]["pubsubbeat"]["json_disable_indexes"].any? { |s| node["hostname"].include?(s) }

topic_name = node["gce"]["instance"]["attributes"]["CHEF_TOPIC_NAME"]
if node["gitlab-elk"]["pubsubbeat"]["elastic5"]
  conf_content["output.elasticsearch"]["index"] = topic_name + "-%{+yyyy.MM.dd}"
  conf_content["setup"]["template.enabled"] = true
  conf_content["setup"]["template.name"] = "%{[agent.name]}-%{[agent.version]}"
  conf_content["setup"]["template.pattern"] = "%{[agent.name]}-%{[agent.version]}-*"
else
  conf_content["output.elasticsearch"]["index"] = topic_name
end

file node["gitlab-elk"]["pubsubbeat"]["conf_file"] do
  content conf_content.to_yaml
  mode '0600'
  sensitive true
  notifies :restart, "runit_service[pubsubbeat]", :delayed
end

file node["gitlab-elk"]["pubsubbeat"]["field_file"] do
  content node["gitlab-elk"]["pubsubbeat"]["fields"].to_a.to_yaml
  mode '0644'
  notifies :restart, "runit_service[pubsubbeat]", :delayed
end

runit_service "pubsubbeat" do
  options(
    binary_path: node["gitlab-elk"]["pubsubbeat"]["bin"],
    conf_file: node["gitlab-elk"]["pubsubbeat"]["conf_file"],
    extra_opts: node["gitlab-elk"]["pubsubbeat"]["extra_opts"]
  )
  sv_timeout 30
  default_logger true
end
