#
# Cookbook Name:: gitlab-elk-kibana
# Recipe:: plugin-x-pack-kibana
# License:: MIT
#
# Copyright 2016, GitLab Inc.
#

bash "install_x_pack_kibana" do
  code <<-EOH
    /usr/share/kibana/bin/kibana-plugin install x-pack
  EOH
  not_if "/usr/share/kibana/bin/kibana-plugin list | grep 'x-pack'"
  only_if { node['gitlab-elk']['kibana']['plugin-x-pack'] }
  notifies :restart, 'service[kibana]'
end
