elast_ver = node['elastalert']['version']
elast_repo = node['elastalert']['repository']
elast_es_host = node['elastalert']['elasticsearch']['hostname']
elast_es_port = node['elastalert']['elasticsearch']['port']
elast_es_index = node['elastalert']['elasticsearch']['index']
elast_es_old_index = node['elastalert']['elasticsearch']['index_old']
elast_es_url_prefix = node['elastalert']['elasticsearch']['url_prefix']
elast_es_index_create_opts = node['elastalert']['elasticsearch']['create_index_opts']
elast_group = node['elastalert']['group']
elast_user = node['elastalert']['user']
elast_user_home = node['elastalert']['user_home']
elast_dir = node['elastalert']['directory']
elast_venv = node['elastalert']['virtualenv']['directory']
elast_config_dir = node['elastalert']['config_dir']
elast_rules_dir = "#{elast_config_dir}/rules"
elast_config_file = "#{elast_config_dir}/config.yaml"

group elast_group

user elast_user do
  group elast_group
  home elast_user_home
  manage_home true
end

directory elast_dir do
  owner elast_user
  group elast_group
  mode '0755'
end

directory elast_config_dir do
  user 'root'
  group 'root'
  mode '0755'
end

directory elast_rules_dir do
  user 'root'
  group 'root'
  mode '0755'
end

template elast_config_file do
  source 'config.yaml.erb'
  owner 'root'
  group 'root'
  mode '0755'
end

include_recipe 'git'

git 'elastalert' do
  repository elast_repo
  revision elast_ver
  destination elast_dir
  user elast_user
  group elast_group
  action :checkout
end

# needed for python
%w(python2.7 python2.7-dev libffi-dev libyaml-0-2).each do |package|
  apt_package package
end

python_runtime '2' # requriment of elastalert

python_virtualenv elast_venv do
  group elast_group
  user elast_user
  not_if { ::File.exist?(elast_venv) }
end

python_execute "#{elast_dir}/setup.py install" do
  user elast_user
  group elast_group
  virtualenv elast_venv
  cwd elast_dir
  not_if { ::File.exist?("#{elast_venv}/bin/elastalert-create-index") }
  notifies :install, "pip_requirements[#{elast_dir}/requirements.txt]", :immediately
end

pip_requirements "#{elast_dir}/requirements.txt" do
  user elast_user
  group elast_group
  virtualenv elast_venv
  options '-v'
  cwd elast_dir
  retries 1 # 1st try fails on clean up step when trying to remove not existing file, 2nd try is successful
  action :nothing
end

python_execute 'setup elastalert index' do
  command "#{elast_venv}/bin/elastalert-create-index --host #{elast_es_host} --port #{elast_es_port} --index '#{elast_es_index}' --url-prefix '#{elast_es_url_prefix}' --old-index '#{elast_es_old_index}' #{elast_es_index_create_opts}"
  user elast_user
  group elast_group
  virtualenv elast_venv
  cwd elast_dir
  # If elasticsearch is down or hasn't started yet we can wait to initialize the index
  only_if "curl -XGET 'http://#{elast_es_host}:#{elast_es_port}/' -s"
  # If elasticsearch is up but the index doesn't exist
  not_if "curl -XGET 'http://#{elast_es_host}:#{elast_es_port}/#{elast_es_index}/' -s | grep '@timestamp'"
end

systemd_unit 'elastalert.service' do
  content <<-EOU.gsub(/^\s+/, '')
  [Unit]
  Description=ElastAlert
  After=network.target

  [Service]
  Type=simple
  User=#{elast_user}
  Group=#{elast_group}
  ExecStart=#{elast_venv}/bin/elastalert --config #{elast_config_file} --verbose
  Restart=on-failure

  [Install]
  WantedBy=multi-user.target
  EOU

  action [ :create, :enable, :start ]
end
