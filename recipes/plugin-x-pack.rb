#
# Cookbook Name:: gitlab-elk
# Recipe:: plugin-x-pack
# License:: MIT
#
# Copyright 2016, GitLab Inc.
#
include_recipe '::plugin-x-pack-kibana'
include_recipe '::plugin-x-pack-logstash'
