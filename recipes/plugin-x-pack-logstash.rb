#
# Cookbook Name:: gitlab-elk-logstash
# Recipe:: plugin-x-pack-logstash
# License:: MIT
#
# Copyright 2016, GitLab Inc.
#

bash "install_x_pack_logstash" do
  code <<-EOH
    /usr/share/logstash/bin/logstash-plugin install x-pack
  EOH
  not_if "/usr/share/logstash/bin/logstash-plugin list | grep 'x-pack'"
  only_if { node['gitlab-elk']['logstash']['plugin-x-pack'] }
  notifies :restart, 'service[logstash]'
end
