require 'spec_helper'
require "chef-vault"

describe "gitlab-elk::pubsubbeat" do
  context "default execution" do
    before do
      allow(Chef::DataBag).to receive(:load).and_return(
        "_default_keys" => { "id" => "_default_keys" }
      )
      allow(ChefVault::Item).to receive(:load).and_return(
        "id" => "_default",
        "gitlab-elk" => {
          "pubsubbeat" => {
            "conf" => {
              "cloud.id" => "some-cloud-id",
              "cloud.auth" => "some-cloud-auth"
            }
          }
        }
      )
    end
    cached(:chef_run) do
      ChefSpec::SoloRunner.new { |node|
        node.normal['gitlab-elk']['pubsubbeat']['project'] = "some-project-id"
        node.normal['gitlab-elk']['pubsubbeat']['json_disable_indexes'] = ["my-nonjson-index", "herp", "derp"]
        node.normal['gitlab-elk']['chef_vault'] = "gitlab-elk"
        node.normal['gce']['instance']['attributes']['CHEF_TOPIC_NAME'] = "Fauxhai"
        node.automatic_attrs['hostname'] = 'my-nonjson-host'
      }.converge(described_recipe)
    end

    it "creates the pubsubbeat home" do
      expect(chef_run).to create_directory("/opt/pubsubbeat").with(mode: '0700')
    end

    it "installs pubsubbeat" do
      expect(chef_run).to put_ark("pubsubbeat")
    end

    it "creates the runit service" do
      expect(chef_run).to enable_runit_service("pubsubbeat")
    end

    it 'creates fields file' do
      expect(chef_run).to create_file('/opt/pubsubbeat/fields.yml').with(mode: '0644')
      expect(chef_run).to(render_file('/opt/pubsubbeat/fields.yml').with_content do |c|
        pubsub_config = <<~FIELDCONFIG
          ---
          - fields:
            - required: false
              type: text
              name: message
            - required: false
              type: object
              name: json
            - required: true
              type: text
              name: message_id
            - required: true
              type: date
              name: publish_time
              format: date
            - required: false
              type: object
              name: attributes
            key: pubsubbeat
            title: pubsubbeat
        FIELDCONFIG
        expect(c).to eq(pubsub_config)
      end)
    end

    it 'creates config file' do
      expect(chef_run).to create_file('/opt/pubsubbeat/pubsubbeat.yml').with(mode: '0600')
      expect(chef_run).to(render_file('/opt/pubsubbeat/pubsubbeat.yml').with_content do |c|
        pubsub_config = <<~PUBCONFIG
          ---
          pubsubbeat:
            project_id: some-project-id
            topic: Fauxhai
            subscription.name: my-nonjson-host-sub
            subscription.retention_duration: 168h
            subscription.connection_pool_size: 10
            json.enabled: true
            json.add_error_key: true
          cloud.id: some-cloud-id
          cloud.auth: some-cloud-auth
          http.enabled: true
          output.elasticsearch:
            enabled: true
            worker: 8
            bulk_max_size: 4096
            compression_level: 0
            index: Fauxhai
          setup:
            ilm.enabled: false
            template.enabled: false
          queue:
            mem:
              events: 2048
          processors:
          - drop_fields:
              fields:
              - agent
              - message
        PUBCONFIG
        expect(c).to eq(pubsub_config)
      end)
    end
  end

  context "json disabled execution" do
    before do
      allow(Chef::DataBag).to receive(:load).and_return(
        "_default_keys" => { "id" => "_default_keys" }
      )
      allow(ChefVault::Item).to receive(:load).and_return(
        "id" => "_default",
        "gitlab-elk" => {
          "pubsubbeat" => {
            "conf" => {
              "cloud.id" => "some-cloud-id",
              "cloud.auth" => "some-cloud-auth"
            }
          }
        }
      )
    end
    cached(:chef_run) do
      ChefSpec::SoloRunner.new { |node|
        node.normal['gitlab-elk']['pubsubbeat']['project'] = "some-project-id"
        node.normal['gitlab-elk']['pubsubbeat']['json_disable_indexes'] = %w[foo nonjson herp derp]
        node.normal['gitlab-elk']['chef_vault'] = "gitlab-elk"
        node.normal['gce']['instance']['attributes']['CHEF_TOPIC_NAME'] = "Fauxhai"
        node.automatic_attrs['hostname'] = 'my-nonjson-host'
      }.converge(described_recipe)
    end

    it 'creates config file with json disabled' do
      expect(chef_run).to create_file('/opt/pubsubbeat/pubsubbeat.yml').with(mode: '0600')
      expect(chef_run).to(render_file('/opt/pubsubbeat/pubsubbeat.yml').with_content do |c|
        pubsub_config = <<~PUBCONFIG
          ---
          pubsubbeat:
            project_id: some-project-id
            topic: Fauxhai
            subscription.name: my-nonjson-host-sub
            subscription.retention_duration: 168h
            subscription.connection_pool_size: 10
            json.enabled: false
            json.add_error_key: true
          cloud.id: some-cloud-id
          cloud.auth: some-cloud-auth
          http.enabled: true
          output.elasticsearch:
            enabled: true
            worker: 8
            bulk_max_size: 4096
            compression_level: 0
            index: Fauxhai
          setup:
            ilm.enabled: false
            template.enabled: false
          queue:
            mem:
              events: 2048
          processors:
          - drop_fields:
              fields:
              - agent
              - message
        PUBCONFIG
        expect(c).to eq(pubsub_config)
      end)
    end
  end

  context "elastic 5 output" do
    before do
      allow(Chef::DataBag).to receive(:load).and_return(
        "_default_keys" => { "id" => "_default_keys" }
      )
      allow(ChefVault::Item).to receive(:load).and_return(
        "id" => "_default",
        "gitlab-elk" => {
          "pubsubbeat" => {
            "conf" => {
              "cloud.id" => "some-cloud-id",
              "cloud.auth" => "some-cloud-auth"
            }
          }
        }
      )
    end
    cached(:chef_run) do
      ChefSpec::SoloRunner.new { |node|
        node.normal['gitlab-elk']['pubsubbeat']['project'] = "some-project-id"
        node.normal['gitlab-elk']['chef_vault'] = "gitlab-elk"
        node.normal['gce']['instance']['attributes']['CHEF_TOPIC_NAME'] = "Fauxhai"
        node.normal['gitlab-elk']['pubsubbeat']['elastic5'] = true
        node.automatic_attrs['hostname'] = 'my-nonjson-host'
      }.converge(described_recipe)
    end

    it 'creates config file' do
      expect(chef_run).to create_file('/opt/pubsubbeat/pubsubbeat.yml').with(mode: '0600')
      expect(chef_run).to(render_file('/opt/pubsubbeat/pubsubbeat.yml').with_content do |c|
        pubsub_config = <<~PUBCONFIG
          ---
          pubsubbeat:
            project_id: some-project-id
            topic: Fauxhai
            subscription.name: my-nonjson-host-sub
            subscription.retention_duration: 168h
            subscription.connection_pool_size: 10
            json.enabled: true
            json.add_error_key: true
          cloud.id: some-cloud-id
          cloud.auth: some-cloud-auth
          http.enabled: true
          output.elasticsearch:
            enabled: true
            worker: 8
            bulk_max_size: 4096
            compression_level: 0
            index: Fauxhai-%{+yyyy.MM.dd}
          setup:
            ilm.enabled: false
            template.enabled: true
            template.name: "%{[agent.name]}-%{[agent.version]}"
            template.pattern: "%{[agent.name]}-%{[agent.version]}-*"
          queue:
            mem:
              events: 2048
          processors:
          - drop_fields:
              fields:
              - agent
              - message
        PUBCONFIG
        expect(c).to eq(pubsub_config)
      end)
    end
  end
end
